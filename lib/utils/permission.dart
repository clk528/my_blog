import 'package:permission_handler/permission_handler.dart';

class PermissHandle {
  static Future<void> requestCamera() async {
      PermissionStatus status = await Permission.camera.status;
      if(status.isUndetermined){
          Map<Permission,PermissionStatus> statuses = await [
            Permission.camera
          ].request();

          print(statuses[Permission.camera]);
      }
  }
}
