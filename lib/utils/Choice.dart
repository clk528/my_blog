import 'package:flutter/material.dart';

class Choice {
  final String title;
  final IconData icon;
  final Map params;

  const Choice(this.title, this.icon, {this.params});
}