class TabItem {
  String name, activeIcon, normalIcon;

  TabItem(this.name, this.normalIcon, this.activeIcon);
}
