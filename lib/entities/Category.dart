class Category {
  int id;
  String name;

  Category({this.id, this.name});

  Category.fromJson(Map<String, dynamic> json) {
    if (json is Map && json.isNotEmpty) {
      this.id = json['id'];
      this.name = json['name'];
    }
  }
}
