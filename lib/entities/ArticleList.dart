import 'package:my_blog/entities/Article.dart';

class ArticleList {
  int currentPage;
  List<Article> data;
  String firstPageUrl;
  int from;
  int lastPage;
  String lastPageUrl;
  String nextPageUrl;
  String path;
  int perPage;
  String prevPageUrl;
  int to;
  int total;

  ArticleList(
      {this.currentPage,
      this.data,
      this.firstPageUrl,
      this.from,
      this.lastPage,
      this.lastPageUrl,
      this.nextPageUrl,
      this.path,
      this.perPage,
      this.prevPageUrl,
      this.to,
      this.total});

  ArticleList.fromJson(Map<String, dynamic> json) {
    if (json is Map && json.isNotEmpty) {
      this.currentPage = json['current_page'];
      this.firstPageUrl = json['first_page_url'];
      this.from = json['from'];
      this.lastPage = json[''];
      this.lastPageUrl = json[''];
      this.nextPageUrl = json[''];
      this.path = json['path'];
      this.perPage = json[''];
      this.prevPageUrl = json[''];
      this.to = json['to'];
      this.total = json['total'];

      this.data =
          json['data'].map<Article>((item) => Article.fromJson(item)).toList();
    }
  }

  Map<String, dynamic> toJson() {
    Map<String, dynamic> json = {
      "currentPage": this.currentPage,
      "data": this.data,
      "firstPageUrl": this.firstPageUrl,
      "from": this.from,
      "lastPage": this.lastPage,
      "lastPageUrl": this.lastPageUrl,
      "nextPageUrl": this.nextPageUrl,
      "path": this.path,
      "perPage": this.perPage,
      "prevPageUrl": this.prevPageUrl,
      "to": this.to,
      "total": this.total
    };
    return json;
  }
}
