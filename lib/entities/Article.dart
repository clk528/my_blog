import 'package:my_blog/entities/Category.dart';
import 'package:my_blog/utils/date_util.dart';

class Article {
  int id;
  String title;
  int categoryId;
  String subtitle;
  int status;
  String createUser;
  String modifyUser;
  String created;
  String modified;
  Category category;

  Article(
      {this.id,
      this.title,
      this.categoryId,
      this.subtitle,
      this.status,
      this.createUser,
      this.modifyUser,
      this.created,
      this.modified,
      this.category});

  Article.fromJson(Map<String, dynamic> json) {
    if (json is Map && json.isNotEmpty) {
      this.id = json['id'];
      this.title = json['title'];
      this.categoryId = json['category_id'];
      this.subtitle = json['subtitle'];
      this.status = json['status'];
      this.createUser = json['createUser'];
      this.modifyUser = json['modifyUser'];

      this.created = DateUtil.toChinaTime(DateTime.fromMillisecondsSinceEpoch(
          int.parse(json['created']) * 1000));
      this.modified = DateUtil.toChinaTime(DateTime.fromMillisecondsSinceEpoch(
          int.parse(json['modified']) * 1000));
      this.category = Category.fromJson(json['category']);
    }
  }
}
