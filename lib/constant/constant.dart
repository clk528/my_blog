class Constant {
  static const START_IMAGE = 'assets/images/home2.jpg';
  static const START_LOADING_TIME = 5;
  static const DEBUG = true;
  static const BASE_URL = 'https://clk528.com';
}
