import 'package:my_blog/constant/constant.dart';
import 'package:my_blog/entities/ArticleList.dart';
import 'package:my_blog/http/DioRequest.dart';

class Api {
  DioRequest _dioRequest;

  final String ARTICLE_LIST = '/api/article';

  Api() {
    _dioRequest = new DioRequest(Constant.BASE_URL);
  }

  Future<ArticleList> getArticleList({Map<String, dynamic> params}) async {
    final Map response = await _dioRequest.post(ARTICLE_LIST, params);
    return ArticleList.fromJson(response);
  }
}
