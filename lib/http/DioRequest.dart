import 'dart:convert' as Convert;
import 'package:dio/dio.dart';
import 'package:my_blog/constant/constant.dart';

class DioRequest {
  Dio _dio;

  DioRequest(String base_url) {
    _dio = new Dio();
    _dio.options.baseUrl = base_url;
    _dio.interceptors.add(InterceptorsWrapper(onResponse: (Response response) {
      return response;
    }));
  }
//发送get请求
  Future<dynamic> get(String uri, {Map<String, String> headers}) async {
    try {
      Response response =
          await _dio.get(uri, options: Options(headers: headers));
      final int statusCode = response.statusCode;
      String body;
      if (response.data is List) {
        body = Convert.json.encode(response.data);
      } else {
        body = response.toString();
      }

      if (Constant.DEBUG) {
        print('[uri=$uri][statusCode=$statusCode][response=$body]');
      }

      return Convert.json.decode(body);
    } on Exception catch (e) {
      print('[uri=$uri]exception e=${e.toString()}');
    }
  }

  //获取返回的内容
  Future<dynamic> getResponseBody(String uri,
      {Map<String, String> headers}) async {
    try {
      Response response =
          await _dio.get(uri, options: Options(headers: headers));

      final int statusCode = response.statusCode;
      String body;
      if (response.data is List) {
        body = Convert.json.encode(response.data);
      } else {
        body = response.toString();
      }

      print('[uri=$uri][statusCode=$statusCode][response=$body]');
      return body;
    } on Exception catch (e) {
      print('[uri=$uri]exception e=${e.toString()}');
    }
  }

  //发送post请求
  Future<dynamic> post(String uri, dynamic data,
      {Map<String, String> headers}) async {
    try {
      Response response =
          await _dio.post(uri, data: data, options: Options(headers: headers));

      final statusCode = response.statusCode;
      String body;
      if (response.data is List) {
        body = Convert.json.encode(response.data);
      } else {
        body = response.toString();
      }

      if (Constant.DEBUG) {
        print('[uri=$uri][statusCode=$statusCode][response=$body]');
      }

      return Convert.json.decode(body);
    } on Exception catch (e) {
      print('[uri=$uri]exception e=${e.toString()}');
    }
  }
}
