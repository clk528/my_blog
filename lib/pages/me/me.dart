import 'package:flutter/material.dart';

class MePage extends StatefulWidget {
  @override
  _MePage createState() => _MePage();
}

class _MePage extends State<MePage> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image.asset('assets/images/home.gif'),
            Text('扭一扭')
          ],
        ),
      ),
    );
  }
}
