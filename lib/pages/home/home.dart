import 'package:flutter/material.dart';
import 'package:badges/badges.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ListView.builder(
      itemCount: 6,
      itemBuilder: (context, index) {
        return getItem(index);
      },
    );
  }

  Widget getItem(int position) {
    String name, desc;
    switch (position) {
      case 0:
        name = '小葵花';
        desc = '你在干嘛呢';
        break;
      case 1:
        name = '酷雪 安安';
        desc = '[你收到一个红包]';
        break;
      case 2:
        name = 'Amy';
        desc = '[图片]';
        break;
      case 3:
        desc = '[语音]';
        name = '马云';
        break;
      case 4:
        desc = '有人@我';
        name = '情色聊天室';
        break;
      case 5:
        desc = '龙哥今晚一起吃饭吧';
        name = '彭于晏';
        break;
    }
    return ChatListItem(position + 1, name, desc);
  }
}

class ChatListItem extends StatefulWidget {
  final String name;
  final String desc;
  final int index;

  ChatListItem(this.index, this.name, this.desc);

  @override
  _ChatListItem createState() => _ChatListItem();
}

class _ChatListItem extends State<ChatListItem> {
  final String defaultAvatar = 'assets/images/avatar.jpg';

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 60.0,
              margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
              child: Row(
                children: <Widget>[
                  Badge(
                    showBadge: widget.index % 5 == 0,
                    elevation: 1,
                    padding: EdgeInsets.all(3),
                    badgeContent: Text(
                      widget.index.toString(),
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 8,
                      ),
                    ),
                    child: Image.asset(
                      defaultAvatar,
                      width: 44.0,
                      height: 44.0,
                    ),
                  ),
                  Expanded(
                    child: Container(
                      height: 40.0,
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            widget.name,
                            style: TextStyle(fontSize: 15.0),
                          ),
                          Text(
                            widget.desc,
                            style: TextStyle(
                                fontSize: 12.0,
                                color: widget.index % 5 == 0
                                    ? Colors.red
                                    : Color(0xffaaaaaa)),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),
                    child: Text(
                      '13:30',
                      style: TextStyle(
                        fontSize: 10.0,
                        color: const Color(0xffaaaaaa),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.fromLTRB(12.0, 0.0, 12.0, 0.0),
              height: 0.5,
              color: Color(0xffebebeb),
            ),
          ],
        ),
      ),
      onTap: () {},
    );
  }
}
