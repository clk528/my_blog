import 'package:flutter/material.dart';
import 'package:my_blog/pages/contact/contact.dart';
import 'package:my_blog/pages/discover/discover.dart';
import 'package:my_blog/pages/home/home.dart';
import 'package:my_blog/pages/me/me.dart';
import 'package:my_blog/utils/Choice.dart';
import 'package:my_blog/utils/TabItem.dart';
import 'package:barcode_scan/barcode_scan.dart';

class TabBar extends StatefulWidget {
  @override
  _TabBar createState() => _TabBar();
}

class _TabBar extends State<TabBar> {
  int _selectIndex = 0;

  final _pageController = PageController(keepPage: true);

  final List<TabItem> itemNames = [
    TabItem('微信', 'assets/images/tab/home.png',
        'assets/images/tab/home-active.png'),
    TabItem('通讯录', 'assets/images/tab/contact.png',
        'assets/images/tab/contact-active.png'),
    TabItem('发现', 'assets/images/tab/discover.png',
        'assets/images/tab/discover-active.png'),
    TabItem('我', 'assets/images/tab/me.png', 'assets/images/tab/me-active.png')
  ];

  final List<Choice> choices = [
    Choice('发起群聊', Icons.group_add),
    Choice('添加朋友', Icons.person_add),
    Choice('扫一扫', Icons.center_focus_weak),
    Choice('收付款', Icons.monetization_on),
    Choice('帮助与反馈', Icons.local_post_office)
  ];

  List<Widget> pages = [HomePage(), ContactPage(), DiscoverPage(), MePage()];

  List<BottomNavigationBarItem> getTab() {
    return itemNames
        .map(
          (TabItem item) => BottomNavigationBarItem(
            icon: Image.asset(
              item.normalIcon,
              width: 30.0,
              height: 30.0,
            ),
            label: item.name,
            activeIcon: Image.asset(
              item.activeIcon,
              width: 30.0,
              height: 30.0,
            ),
          ),
        )
        .toList();
  }

  List<PopupMenuItem> getChoice() {
    return choices
        .map(
          (Choice item) => PopupMenuItem(
            value: item,
            child: Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Icon(item.icon, color: Colors.white),
                    Padding(
                      padding: EdgeInsets.only(left: 10.0),
                    ),
                    Text(
                      item.title,
                      style: TextStyle(color: Colors.white),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 30.0),
                  child: Divider(color: Colors.white30),
                )
              ],
            ),
          ),
        )
        .toList();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromARGB(255, 248, 248, 248),
      appBar: PreferredSize(
        child: Offstage(
          offstage: itemNames[_selectIndex].name == '我',
          child: AppBar(
            backgroundColor: Colors.blueGrey[50],
            bottomOpacity: 0,
            elevation: 0.5,
            title: Text(itemNames[_selectIndex].name),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () {
                  print(111);
                },
              ),
              Theme(
                data: Theme.of(context).copyWith(
                  cardColor: Colors.black54,
                ),
                child: PopupMenuButton(
                  offset: Offset(0.0, 130.0),
                  icon: Icon(Icons.add_circle_outline),
                  itemBuilder: (BuildContext context) => getChoice(),
                  onSelected: (item) {
                    if (item.title == '扫一扫') {
                      scan();
                    }
                  },
                ),
              )
              // IconButton(
              //   icon: Icon(Icons.add_circle_outline), onPressed: () {
              //     print('photo_camera');
              //   },
              // )
            ],
          ),
        ),
        preferredSize:
            Size.fromHeight(MediaQuery.of(context).size.height * 0.07),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: getTab(),
        currentIndex: _selectIndex,
        fixedColor: Color.fromARGB(255, 68, 181, 73),
        backgroundColor: Colors.white,
        type: BottomNavigationBarType.fixed,
        onTap: (int index) {
          print(itemNames[index].name);

          setState(() {
            _selectIndex = index;
          });
          _pageController.jumpToPage(index);
        },
      ),
      body: PageView(
        controller: _pageController,
        onPageChanged: (int index) {
          setState(() {
            _selectIndex = index;
          });
        },
        children: pages,
        // physics: NeverScrollableScrollPhysics(), // 禁止滑动
      ),
    );
  }

  void scan() {
    BarcodeScanner.scan().then((ScanResult result) {
      showDialog<void>(
        context: context,
        barrierDismissible: false, // user must tap button!
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('扫描'),
            content: SingleChildScrollView(
              child: Text(result.rawContent),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('确定'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
    }).catchError((error) {
      print('error');
    });
  }
}
