import 'package:flutter/material.dart';
import 'package:my_blog/constant/constant.dart';
import 'package:my_blog/pages/CountDownWidget.dart';
import 'package:my_blog/pages/TabBar.dart' as AppTabBar;
import 'package:my_blog/utils/screen_utils.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  bool showStartPage = true;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Offstage(
          child: AppTabBar.TabBar(),
          offstage: showStartPage,
        ),
        Offstage(
          child: Container(
            color: Colors.white,
            width: ScreenUtils.screenW(context),
            height: ScreenUtils.screenH(context),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment(0.0, 0.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CircleAvatar(
                        radius: ScreenUtils.screenW(context) / 3,
                        backgroundColor: Colors.white,
                        backgroundImage: AssetImage(Constant.START_IMAGE),
                      ),
                      // Padding(
                      //   padding: const EdgeInsets.only(top: 20.0),
                      //   child: Text(
                      //     '来自老陈的爱心app',
                      //     style: TextStyle(fontSize: 18.0, color: Colors.black),
                      //   ),
                      // )
                    ],
                  ),
                ),
                SafeArea(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Align(
                        alignment: Alignment(1.0, 0.0),
                        child: Container(
                          margin: EdgeInsets.only(
                            right: 30.0,
                            top: 20.0,
                          ),
                          padding: EdgeInsets.only(
                            left: 10.0,
                            right: 10.0,
                            top: 2.0,
                            bottom: 2.0,
                          ),
                          child: CountDownWidget(
                            onCountDownFinishCallBack: (bool value) {
                              setState(() {
                                showStartPage = false;
                              });
                            },
                            seconds: Constant.START_LOADING_TIME,
                          ),
                          decoration: BoxDecoration(
                            color: Color(0xffEDEDED),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10.0),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 40.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: Text(
                                '你好，小居居',
                                style: TextStyle(
                                  color: Color.fromRGBO(143, 195, 31, 1),
                                  fontSize: 30.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          offstage: !showStartPage,
        )
      ],
    );
  }
}
